/*   Exercício EAD -- Banco de Dados "Empresa" -- Script de Consulta
 * 
 * Obtenha os dados dos funcionários cujo nome inicie com L ou termine com A, 
 * sejam do sexo masculino, tenham sido admitidos em outubro de 2000 e seu 
 * salario seja maior do que a sua comissão.
 **/

SELECT * FROM funcionario WHERE 
( nome_fun LIKE 'L%' ) AND  ( nome_fun LIKE '%a' ) AND 
( upper(sexo) = 'M' ) AND 
( data_admissao BETWEEN '2000-10-01' AND '2000-10-31' ) AND 
( salario > comissao );

