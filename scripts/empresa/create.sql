/*                       Banco de Dados Empresa
 *
 * Script para Postgres importado do banco de dados criado pelo professor
 * Sidney Vieira em <http://pgsql.sidneyvieira.kinghost.net/>
 *
 **/

-- Codificação
SET client_encoding = 'UTF8';


/* Criação das tabelas */

-- Departamento
CREATE TABLE departamento (
    cod_depto integer NOT NULL,
    sigla character varying(2),
    nome character varying(20),
    orcamento numeric(10,2)
);

-- Funcionário
CREATE TABLE funcionario (
    cod_fun integer NOT NULL,
    nome_fun character varying(30),
    salario numeric(6,2),
    comissao numeric(6,2),
    sexo character varying(1),
    data_admissao date,
    cod_depto integer NOT NULL
);


/* Inserção dos dados no banco */

-- Departamentos
COPY departamento (cod_depto, sigla, nome, orcamento) FROM stdin;
1	D1	DESENVOLVIMENTO	10000.00
2	D2	REDES	20000.00
3	D3	MANUTENCAO	5000.00
4	D4	OPERACAO	2000.00
5	D5	WEB&DESIGN	15000.00
\.


-- Funcionários
COPY funcionario (cod_fun, nome_fun, salario, comissao, sexo, data_admissao, cod_depto) FROM stdin;
4	Lea Pereira	1500.00	110.00	F	2000-10-23	2
3	Silvia Pereira	1000.00	200.00	F	2000-10-23	3
5	Luiza Pereira	1000.00	200.00	F	2000-10-23	2
7	Luiza Costa	950.00	300.00	F	2001-03-02	1
8	Adolfo Costa	1950.00	150.00	M	2001-03-02	1
9	Antonio alves	1800.50	200.00	M	2000-10-15	1
6	Ze Pereira	900.00	110.00	M	2000-10-23	3
10	Antonia silva	1850.50	230.00	F	2000-11-15	2
11	Elza silva	850.50	310.00	F	2000-11-25	3
2	Pedro da Silva	600.00	800.00	M	2001-02-03	1
12	Elza silva	850.50	310.00	F	2000-11-25	3
1	Sidney da Silva	450.00	900.00	M	2001-02-03	1
\.


/* Indicação das chaves */

-- Chave primária de Departamento
ALTER TABLE ONLY departamento
    ADD CONSTRAINT pk_departamento PRIMARY KEY (cod_depto);

-- Chave primária de Funcionário
ALTER TABLE ONLY funcionario
    ADD CONSTRAINT pk_funcionario PRIMARY KEY (cod_fun);

-- Chave estrangeira do departamento em funcionário
ALTER TABLE ONLY funcionario
    ADD CONSTRAINT fk_funcionario FOREIGN KEY (cod_depto) REFERENCES departamento(cod_depto);

