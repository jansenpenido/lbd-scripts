SELECT Cliente.nome AS "Nome do Cliente", 
	Cliente.data_cadastro AS "Data de Cadastro", 
	Cliente.sexo AS "Sexo", 
	Venda.numero AS "Nº da Venda", 
	Venda.valor AS "Valor" 
FROM Cliente INNER JOIN Venda 
ON Cliente.codigo = Venda.cod_cli 
WHERE ( UPPER(Cliente.sexo) = 'M')
	AND (Venda.valor BETWEEN 1000 AND 2000) 
	AND ( (Cliente.nome LIKE 'S%') 
		OR (Cliente.nome LIKE 'R%') 
		OR (Cliente.nome LIKE 'S%') ) 
	AND ( (Cliente.data_cadastro BETWEEN '2009-1-1' AND '2009-1-31') 
		OR (Cliente.data_cadastro BETWEEN '2009-7-1' AND '2009-7-31') );

