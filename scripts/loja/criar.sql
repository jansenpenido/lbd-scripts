/*        Laboratório de Banco de Dados -- Sistema Comercial
 * 
 * Script para criação de um banco de dados comercial.
 * 
 **/

/* Configurar... */
SET client_encoding = 'UTF8';


/* Criar domínios... */
CREATE DOMAIN T_sexo AS CHAR(1) 
	CHECK( VALUE IN('f','m') );

CREATE DOMAIN T_venda AS VARCHAR(2) 
	CHECK( VALUE IN('di','ch','ca') ) DEFAULT 'di';


/* Criar tabelas... */

-- Cliente 
CREATE TABLE Cliente(
	codigo INT NOT NULL,
	nome VARCHAR(20) NOT NULL,
	data_cadastro DATE NOT NULL DEFAULT NOW(),
	sexo T_sexo,
	CONSTRAINT PK_cliente PRIMARY KEY(codigo)
);

-- Venda
CREATE TABLE Venda(
	numero INT NOT NULL,
	data DATE NOT NULL,
	valor DECIMAL(10,2) NOT NULL,
	cod_cli INT NOT NULL,
	tipo T_venda NOT NULL,
	CONSTRAINT PK_venda PRIMARY KEY(numero),
	CONSTRAINT FK_cliente FOREIGN KEY(cod_cli) 
		REFERENCES Cliente(codigo) ON DELETE SET NULL
);
	
-- Produto
CREATE TABLE Produto(
	codigo INT NOT NULL,
	descricao VARCHAR(20) NOT NULL,
	quant_disponivel INT NOT NULL DEFAULT 0,
	CONSTRAINT PK_produto PRIMARY KEY(codigo)
);

-- Contém
CREATE TABLE Contem(
	codigo INT NOT NULL,
	numero INT NOT NULL,
	quantidade INT NOT NULL,
	CONSTRAINT FK_produto FOREIGN KEY(codigo) 
		REFERENCES Produto(codigo) ON DELETE CASCADE,
	CONSTRAINT FK_venda FOREIGN KEY(numero) 
		REFERENCES Venda(numero) ON DELETE CASCADE,
	CONSTRAINT PK_contem PRIMARY KEY(codigo, numero)
);

