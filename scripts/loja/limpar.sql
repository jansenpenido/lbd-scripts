/*        Laboratório de Banco de Dados -- Sistema Comercial
 * 
 * Script para limpar todos os registros de um banco de dados comercial.
 * 
 **/

-- Excluir tabelas
DROP TABLE Contem;
DROP TABLE Venda;
DROP TABLE Produto;
DROP TABLE Cliente;

-- Excluir domínios
DROP DOMAIN T_sexo;
DROP DOMAIN T_venda;

