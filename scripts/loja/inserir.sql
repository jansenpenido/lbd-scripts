/*        Laboratório de Banco de Dados -- Sistema Comercial
 * 
 * Script para inserção de um banco de dados comercial.
 * 
 **/
 
-- Clientes
COPY Cliente(codigo, nome, data_cadastro, sexo) FROM stdin;
1	Jansen Penido	'2010-4-28'	m
2	Nina Santos	'2009-5-3'	f
3	Luana Ferreira	'2011-12-17'	f
4	Pedro Rocha	'2010-12-29'	m
5	Mirian Carvalho	'2010-8-10'	f
6	João Silva	'2010-7-10'	m
7	Cássia Borges	'2010-5-23'	f
8	Rogério Castro	'2009-1-18'	m
\.

-- Vendas
COPY Venda(numero, data, valor, cod_cli, tipo) FROM stdin;
5201	'2011-10-7'	2.99	1	di
5202	'2012-1-29'	1280.84	3	ca
5203	'2012-3-10'	530.10	3	ch
5204	'2011-10-28'	4829.35	3	ch
5205	'2011-2-5'	234.70	4	di
5206	'2011-11-29'	1543.49	4	ca
5207	'2011-4-7'	2312.85	4	ca
5208	'2011-12-20'	1002.15	4	ca
5209	'2012-1-14'	1930.48	5	ch
5210	'2012-4-3'	89.40	6	di
5211	'2010-11-8'	1409.39	6	ca
5212	'2012-4-4'	1250.99	8	ch
\.

-- Produtos
COPY Produto(codigo, descricao, quant_disponivel) FROM stdin;
101	processador	5
102	drive DVD	9
103	teclado multimídia	43
104	fone de ouvido	28
105	mouse USB	29
106	monitor LED	15
107	cabo HDMI	17
108	pilha AA	164
109	roteador sem fio	10
\.

-- Contém
COPY Contem(codigo, numero, quantidade) FROM stdin;
101	5202	1
101	5207	3
102	5202	1
103	5202	1
103	5211	2
104	5205	1
105	5202	1
106	5204	3
106	5208	1
106	5209	2
107	5210	1
107	5203	5
108	5203	8
108	5201	4
106	5212	1
\.

