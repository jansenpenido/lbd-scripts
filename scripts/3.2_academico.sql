/* EAD - Laboratório de Banco de Dados
 * 3.2 Exercicio DML 1 - Criação e Carga (Banco de Dados Acadêmico)
 **/

-- Tabelas
create table aluno(
nr int primary key,
nome varchar(50),
sexo varchar(1));

create table prof(
matr int primary key,
nome varchar(50));

create table cadeira(
cod varchar(5) primary key,
nome varchar(40),
curso varchar(5),
matr int,
constraint PKprof foreign key (matr) references prof(matr));

create table prova(
nr int,
cod varchar(5),
data date,
nota int,
constraint FKprova foreign key(cod) references cadeira(cod),
constraint FKaluno foreign key(nr) references aluno(nr),
constraint prova_pk primary key( nr, cod, data));

-- Carga
insert into aluno values(100, 'João da Silva', 'M');
insert into aluno values(110, 'Manuel Santos',  'M');
insert into aluno values(120, 'Rui Pedro Azeredo', 'M');
insert into aluno values(130, 'Abel Zuan', 'M');
insert into aluno values(140, 'Fernando Abreu', 'M');
insert into aluno values(150, 'Ismael', 'M');
insert into prof values(111, 'Eugênio Silva Abreu');
insert into prof values(222, 'Fernando Dias');
insert into prof values(225, 'João Silva');
insert into cadeira values('TS1', 'Teoria dos Sistemas 1', 'SI', 111);
insert into cadeira values('BD', 'Bases de Dados', 'SI', 111);
insert into cadeira values('EIA', 'Estruturas de Informação e Algoritmos', 'SI', 225);
insert into cadeira values('EP', 'Electrônica de Potência', 'EN', 222);
insert into cadeira values('IE', 'Instalações Elétricas', 'EN', 222);
insert into prova values(100, 'TS1', '2010-02-11', 8);
insert into prova values(100, 'TS1', '2011-02-02', 10);
insert into prova values(100, 'BD', '2011-02-04', 5);
insert into prova values(100, 'EIA', '2010-01-29', 6);
insert into prova values(100, 'EIA', '2011-02-02', 5);
insert into prova values(110, 'EP', '2010-01-30', 8);
insert into prova values(110, 'IE', '2010-02-05', 10);
insert into prova values(110, 'IE', '2011-02-01', 5);
insert into prova values(120, 'TS1', '2011-01-31', 4);
insert into prova values(120, 'EP', '2011-02-04', 4);
insert into prova values(130, 'BD', '2011-02-04', 4);
insert into prova values(130, 'EIA', '2011-02-02', 7);
insert into prova values(130, 'TS1', '2010-02-11', 8);
insert into prova values(140, 'TS1', '2011-01-31', 10);
insert into prova values(140, 'TS1', '2010-02-11', 5);
insert into prova values(140, 'EIA', '2011-02-02', 3);
insert into prova values(150, 'TS1', '2010-02-11', 10);
insert into prova values(150, 'EP', '2011-02-02', 2);
insert into prova values(150, 'BD', '2011-02-04', 9);
insert into prova values(150, 'EIA', '2010-01-29', 5);
insert into prova values(150, 'IE', '2011-02-02', 9);

