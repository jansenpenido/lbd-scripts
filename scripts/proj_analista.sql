/* Banco de Dados Projeto-Analista	
 **/
 
-- Tabelas
create table Analista (
id serial primary key,
nome varchar(50),
sexo char(1) );

create table Projeto (
id serial primary key,
nome varchar(50) not null,
valor money not null,
datainicio date default now(),
datafinal date );

-- Domínios
create domain T_area as varchar(5) check( value in('infra', 'desen') );
alter table Projeto add column area T_area;

-- Chaves Estrangeiras
alter table Projeto add column id_analista_chefia integer;
alter table Projeto add foreign key(id_analista_chefia) references Analista(id) on delete set null;

alter table Analista add column id_proj_atua integer;
alter table Analista add foreign key(id_proj_atua) references Projeto(id) on delete set null;

-- Inserir dados
insert into projeto(nome, valor) values('Proj A', '25000,00');
insert into projeto(nome, valor) values('Proj B', '10000,00');

insert into analista(nome, sexo, id_proj_atua) values('Jansen', 'm', '1');
insert into analista(nome, sexo, id_proj_atua) values('Luana', 'f', '1');
insert into analista(nome, sexo, id_proj_atua) values('Karen', 'f', '2');
insert into analista(nome, sexo, id_proj_atua) values('Ana', 'f', '2');
insert into analista(nome, sexo, id_proj_atua) values('Pedro', 'm', '2');

update projeto set id_analista_chefia = '2' where id = '1';
update projeto set id_analista_chefia = '3' where id = '2';

