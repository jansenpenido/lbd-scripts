/* Laboratório de Banco de Dados Exercício 2.4 - DDL
 **/

-- Tabelas
CREATE TABLE albuns (
    id integer NOT NULL,
    artista integer NOT NULL,
    midia integer NOT NULL,
    nome character varying(40),
    faixas integer,
    ano integer
);

CREATE TABLE artistas (
    id integer NOT NULL,
    nome character varying(40)
);

CREATE TABLE midias (
    id integer NOT NULL,
    nome character varying(20)
);

-- Restrições
ALTER TABLE albuns
    ADD CONSTRAINT PK_albuns PRIMARY KEY (id);

ALTER TABLE  albuns
    ADD CONSTRAINT UQ_albuns UNIQUE (artista, midia);

ALTER TABLE artistas
    ADD CONSTRAINT PK_artistas_pk PRIMARY KEY (id);

ALTER TABLE  midias
    ADD CONSTRAINT PK_midias_pk PRIMARY KEY (id);

ALTER TABLE  albuns
    ADD CONSTRAINT FK_album_artista FOREIGN KEY (artista) REFERENCES artistas(id);

ALTER TABLE albuns
    ADD CONSTRAINT FK_album_midia FOREIGN KEY (midia) REFERENCES midias(id);
