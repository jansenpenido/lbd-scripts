/* [EAD] 2.3 - Exercício DLL - Sistema Universitário  
 **/

CREATE TABLE Aluno (
matricula SERIAL,
nome VARCHAR(40) NOT NULL,
sexo VARCHAR(1) NOT NULL,
CONSTRAINT PK_aluno PRIMARY KEY(matricula) );

CREATE TABLE Professor (
matricula SERIAL,
nome VARCHAR(40) NOT NULL,
data_admissao DATE default now(),
CONSTRAINT PK_prof PRIMARY KEY(matricula) );

CREATE TABLE Disciplina (
codigo SERIAL,
nome VARCHAR(15) NOT NULL,
carga_horaria INTEGER NOT NULL,
matric_professor INTEGER,
CONSTRAINT PK_disciplina PRIMARY KEY(codigo),
CONSTRAINT FK_discip_prof FOREIGN KEY(matric_professor) REFERENCES professor(matricula) ON DELETE SET NULL );

CREATE TABLE Cursa (
codigo SERIAL,
matricula INTEGER NOT NULL,
nota INTEGER NOT NULL,
CONSTRAINT FK_cursa_aluno FOREIGN KEY(matricula) REFERENCES aluno(matricula) ON DELETE CASCADE,
CONSTRAINT FK_cursa_disciplina FOREIGN KEY(codigo) REFERENCES disciplina(codigo) ON DELETE CASCADE );

