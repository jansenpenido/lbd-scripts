/* Exemplo de usos do comando INSERT.
 **/

-- Esquema
CREATE TABLE aluno (
nome VARCHAR(50) NOT NULL,
sexo VARCHAR(1) );

-- Inserções
INSERT INTO aluno VALUES('Ze', 'm');
INSERT INTO aluno(nome) VALUES('Eva');
INSERT INTO aluno(sexo, nome) VALUES('f', 'Ema');

SELECT * FROM aluno;

