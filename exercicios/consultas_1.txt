MSB, 27 de março de 2012

Exercícios: Gere consultas SQL para...

1. Obter o maior valor de salário pago.
		SELECT MAX(salario) FROM Funcionario;

2. Obter a média salário dos funcionários.
		SELECT AVG(salario) FROM Funcionario;

3. Qual o valor total gasto com pagamentos de salários.
		SELECT SUM(salario) FROM Funcionario;

4. Quantos funcionários existem?
		SELECT COUNT(*) FROM Funcionario;
	
5. Quantos departamentos possuem orçamento superior a 10 mil reais.
		SELECT SUM(salario) FROM Funcionario;

6. Qual o valor total gasto com pagamentos de salários para funcionários do sexo 
   feminino.
		SELECT SUM(salario) FROM Funcionario WHERE upper(sexo) = 'F';

7. Qual o valor total gasto com pagamento dos funcionários do departamento 
   código 1 ou 3.
		SELECT SUM(salario) FROM Funcionario 
		WHERE cod_depto = '1' OR cod_depto = '3';
	
8. Quantos funcionários são do departamento 1 ou 3 e são sexo masculino.
		SELECT COUNT(*) FROM Funcionario 
		WHERE (cod_depto = 1 OR cod_depto = 3) AND (upper(sexo) = 'M');

